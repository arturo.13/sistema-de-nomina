-- Database: nomina
DROP TABLE IF EXISTS Users;
DROP TABLE IF EXISTS Workers;
DROP TABLE IF EXISTS WorkPositions;
DROP TABLE IF EXISTS Areas;
DROP TABLE IF EXISTS Roles;

---- Tabla puestos
CREATE TABLE WorkPositions(
    id serial NOt NULL,
    name varchar(32) NOT NULL,
    description varchar(256),
    level_access int NOT NULL, --- 0,1,2 (- ->+)
    PRIMARY KEY (id)
);

CREATE TABLE Areas(
    id serial NOT NULL,
    name varchar(32) NOT NULL,
    description varchar(256),
    PRIMARY KEY (id)
);

CREATE TABLE Roles(
    id serial NOT NULL,
    name varchar(32),
    PRIMARY KEY (id)
);

CREATE TABLE Workers(
    id serial NOT NULL,
    name varchar(20) NOT NULL,
    last_names varchar(40) NOT NULL,
    rfc varchar(13),
    curp varchar(18) NOT NULL,
    birthday date NOT NULL,
    contact_number varchar(10) NOT NULL,
    address varchar(64) NOT NULL,
    email varchar(32) NOT NULL,
    id_position int,
    id_area int,
    salary float,
    haring_date date NOT NULL,
    hr_week int,
    hr_day int,
    PRIMARY KEY (id),
    FOREIGN KEY (id_position) REFERENCES WorkPositions(id),
    FOREIGN KEY (id_area) REFERENCES Areas(id)
);

CREATE TABLE Users(
    id serial NOT NULL,
    name varchar(16) NOT NULL,
    passhash varchar(256) NOT NULL,
    salt varchar(8) NOT NULL,
    id_role int NOT NULL,
    id_worker int NOT NULL,
    UNIQUE (name),
    UNIQUE (id_worker),
    PRIMARY KEY (id),
    FOREIGN KEY (id_role) REFERENCES Roles(id),
    FOREIGN KEY (id_worker) REFERENCES Workers(id)
);

INSERT INTO public.areas (name, description) VALUES ('Docente','Donde se desempeñan los profesores y maestros');
INSERT INTO public.areas (name, description) VALUES ('Directiva','Donde se desempeña la dirección y secretarias educativas');
INSERT INTO public.areas (name, description) VALUES ('Vigilancia','Cuerpo de vigilancia matutina y despertina');

INSERT INTO public.roles (name) VALUES ('Administrador');
INSERT INTO public.roles (name) VALUES ('Editor');
INSERT INTO public.roles (name) VALUES ('Recursos Humanos');

INSERT INTO public.workpositions (name, description, level_access) VALUES ('Director','Jefe de toda la unidad o plantel educativo', 5);
INSERT INTO public.workpositions (name, description, level_access) VALUES ('Docente tipo A','Profesor a tiempo completo' ,3);
INSERT INTO public.workpositions (name, description, level_access) VALUES ('Profesor tipo B','Profesor a medio tiempo' ,2);
INSERT INTO public.workpositions (name, description, level_access) VALUES ('Secretario Académico','Encargado de manejar todo lo referente con los alumnos y profesores' ,4);
INSERT INTO public.workpositions (name, description, level_access) VALUES ('Vigilante tipo A','Vigilante matutitno' ,1);

INSERT INTO public.workers (name, last_names, rfc, curp, birthday, contact_number, address, email, id_position, id_area, salary, haring_date, hr_week, hr_day) VALUES ('Arturo','García García','GAGA871005DV7','GAGA871005GTHRRR02','1987-10-05', '4621253876', 'Guipuzcoa 3937 Col. Bernardo Cobos C.P. 36610', 'agarciag@uvaq.edu.mx',2 ,1, 15000, '2020-01-31',20,4);