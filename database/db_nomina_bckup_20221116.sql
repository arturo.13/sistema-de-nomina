--
-- PostgreSQL database dump
--

-- Dumped from database version 14.4
-- Dumped by pg_dump version 14.4

-- Started on 2022-11-16 11:54:08

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 212 (class 1259 OID 18777)
-- Name: areas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.areas (
    id integer NOT NULL,
    name character varying(32) NOT NULL,
    description character varying(256)
);


ALTER TABLE public.areas OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 18776)
-- Name: areas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.areas_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.areas_id_seq OWNER TO postgres;

--
-- TOC entry 3357 (class 0 OID 0)
-- Dependencies: 211
-- Name: areas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.areas_id_seq OWNED BY public.areas.id;


--
-- TOC entry 214 (class 1259 OID 18784)
-- Name: roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.roles (
    id integer NOT NULL,
    name character varying(32)
);


ALTER TABLE public.roles OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 18783)
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.roles_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.roles_id_seq OWNER TO postgres;

--
-- TOC entry 3358 (class 0 OID 0)
-- Dependencies: 213
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;


--
-- TOC entry 218 (class 1259 OID 18808)
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id integer NOT NULL,
    name character varying(16) NOT NULL,
    passhash character varying(256) NOT NULL,
    salt character varying(8) NOT NULL,
    id_role integer NOT NULL,
    id_worker integer NOT NULL
);


ALTER TABLE public.users OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 18807)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- TOC entry 3359 (class 0 OID 0)
-- Dependencies: 217
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- TOC entry 216 (class 1259 OID 18791)
-- Name: workers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.workers (
    id integer NOT NULL,
    name character varying(20) NOT NULL,
    last_names character varying(40) NOT NULL,
    rfc character varying(13),
    curp character varying(18) NOT NULL,
    birthday date NOT NULL,
    contact_number character varying(10) NOT NULL,
    address character varying(64) NOT NULL,
    email character varying(32) NOT NULL,
    id_position integer,
    id_area integer,
    salary double precision,
    haring_date date NOT NULL,
    hr_week integer,
    hr_day integer
);


ALTER TABLE public.workers OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 18790)
-- Name: workers_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.workers_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.workers_id_seq OWNER TO postgres;

--
-- TOC entry 3360 (class 0 OID 0)
-- Dependencies: 215
-- Name: workers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.workers_id_seq OWNED BY public.workers.id;


--
-- TOC entry 210 (class 1259 OID 18770)
-- Name: workpositions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.workpositions (
    id integer NOT NULL,
    name character varying(32) NOT NULL,
    description character varying(256),
    level_access integer NOT NULL
);


ALTER TABLE public.workpositions OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 18769)
-- Name: workpositions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.workpositions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.workpositions_id_seq OWNER TO postgres;

--
-- TOC entry 3361 (class 0 OID 0)
-- Dependencies: 209
-- Name: workpositions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.workpositions_id_seq OWNED BY public.workpositions.id;


--
-- TOC entry 3185 (class 2604 OID 18780)
-- Name: areas id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.areas ALTER COLUMN id SET DEFAULT nextval('public.areas_id_seq'::regclass);


--
-- TOC entry 3186 (class 2604 OID 18787)
-- Name: roles id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);


--
-- TOC entry 3188 (class 2604 OID 18811)
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- TOC entry 3187 (class 2604 OID 18794)
-- Name: workers id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.workers ALTER COLUMN id SET DEFAULT nextval('public.workers_id_seq'::regclass);


--
-- TOC entry 3184 (class 2604 OID 18773)
-- Name: workpositions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.workpositions ALTER COLUMN id SET DEFAULT nextval('public.workpositions_id_seq'::regclass);


--
-- TOC entry 3345 (class 0 OID 18777)
-- Dependencies: 212
-- Data for Name: areas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.areas (id, name, description) FROM stdin;
1	Docente	Donde se desempeñan los profesores y maestros
2	Directiva	Donde se desempeña la dirección y secretarias educativas
3	Vigilancia	Cuerpo de vigilancia matutina y despertina
\.


--
-- TOC entry 3347 (class 0 OID 18784)
-- Dependencies: 214
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.roles (id, name) FROM stdin;
1	Administrador
2	Editor
3	Recursos Humanos
\.


--
-- TOC entry 3351 (class 0 OID 18808)
-- Dependencies: 218
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, name, passhash, salt, id_role, id_worker) FROM stdin;
\.


--
-- TOC entry 3349 (class 0 OID 18791)
-- Dependencies: 216
-- Data for Name: workers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.workers (id, name, last_names, rfc, curp, birthday, contact_number, address, email, id_position, id_area, salary, haring_date, hr_week, hr_day) FROM stdin;
1	Arturo	García García	GAGA871005DV7	GAGA871005GTHRRR02	1987-10-05	4621253876	Guipuzcoa 3937 Col. Bernardo Cobos C.P. 36610	agarciag@uvaq.edu.mx	2	1	15000	2020-01-31	20	4
\.


--
-- TOC entry 3343 (class 0 OID 18770)
-- Dependencies: 210
-- Data for Name: workpositions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.workpositions (id, name, description, level_access) FROM stdin;
1	Director	Jefe de toda la unidad o plantel educativo	5
2	Docente tipo A	Profesor a tiempo completo	3
3	Profesor tipo B	Profesor a medio tiempo	2
4	Secretario Académico	Encargado de manejar todo lo referente con los alumnos y profesores	4
5	Vigilante tipo A	Vigilante matutitno	1
\.


--
-- TOC entry 3362 (class 0 OID 0)
-- Dependencies: 211
-- Name: areas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.areas_id_seq', 3, true);


--
-- TOC entry 3363 (class 0 OID 0)
-- Dependencies: 213
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.roles_id_seq', 3, true);


--
-- TOC entry 3364 (class 0 OID 0)
-- Dependencies: 217
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 1, false);


--
-- TOC entry 3365 (class 0 OID 0)
-- Dependencies: 215
-- Name: workers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.workers_id_seq', 1, true);


--
-- TOC entry 3366 (class 0 OID 0)
-- Dependencies: 209
-- Name: workpositions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.workpositions_id_seq', 5, true);


--
-- TOC entry 3192 (class 2606 OID 18782)
-- Name: areas areas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.areas
    ADD CONSTRAINT areas_pkey PRIMARY KEY (id);


--
-- TOC entry 3194 (class 2606 OID 18789)
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- TOC entry 3198 (class 2606 OID 18813)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 3196 (class 2606 OID 18796)
-- Name: workers workers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.workers
    ADD CONSTRAINT workers_pkey PRIMARY KEY (id);


--
-- TOC entry 3190 (class 2606 OID 18775)
-- Name: workpositions workpositions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.workpositions
    ADD CONSTRAINT workpositions_pkey PRIMARY KEY (id);


--
-- TOC entry 3201 (class 2606 OID 18814)
-- Name: users users_id_role_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_id_role_fkey FOREIGN KEY (id_role) REFERENCES public.roles(id);


--
-- TOC entry 3202 (class 2606 OID 18819)
-- Name: users users_id_worker_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_id_worker_fkey FOREIGN KEY (id_worker) REFERENCES public.workers(id);


--
-- TOC entry 3200 (class 2606 OID 18802)
-- Name: workers workers_id_area_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.workers
    ADD CONSTRAINT workers_id_area_fkey FOREIGN KEY (id_area) REFERENCES public.areas(id);


--
-- TOC entry 3199 (class 2606 OID 18797)
-- Name: workers workers_id_position_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.workers
    ADD CONSTRAINT workers_id_position_fkey FOREIGN KEY (id_position) REFERENCES public.workpositions(id);


-- Completed on 2022-11-16 11:54:08

--
-- PostgreSQL database dump complete
--

