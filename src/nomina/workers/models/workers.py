from nomina import db

from flask_wtf import FlaskForm
from wtforms import StringField,  IntegerField, DateField, FloatField
from wtforms.validators import InputRequired, Length, Regexp, NumberRange, Optional

#from nomina.utils.widget_date import DatePickerWidget

class Worker(db.Model):
    """
    ____________________ Data in DB
    id serial NOT NULL,
    name varchar(20) NOT NULL,
    last_names varchar(40) NOT NULL,
    rfc varchar(13),
    curp varchar(18) NOT NULL,
    birthday date NOT NULL,
    contact_number varchar(10) NOT NULL,
    address varchar(64) NOT NULL,
    email varchar(32) NOT NULL,
    id_position int,
    id_area int,
    salary float,
    haring_date date NOT NULL,
    hr_week int,
    hr_day int,
    """
    ## .... 
    __tablename__ = 'workers'
    ## ..... variables ...
    #Varibales: db.Column(db.Integer), db.Column(db.Float), db.Column(db.DateTime), db.Column(db.String(8)), db.Column(db.Date), db.Column(db.Boolean)
    id = db.Column(db.Integer, primary_key = True) #serial NOT NULL,
    name = db.Column(db.String(20))#varchar(20) NOT NULL,
    last_names = db.Column(db.String(40))#varchar(40) NOT NULL,
    rfc = db.Column(db.String(13))#varchar(13),
    curp = db.Column(db.String(18))#varchar(18) NOT NULL,
    birthday = db.Column(db.DateTime)#date NOT NULL,
    contact_number = db.Column(db.String(10))#varchar(10) NOT NULL,
    address = db.Column(db.String(64))#varchar(64) NOT NULL,
    email = db.Column(db.String(32))#varchar(32) NOT NULL,
    id_position = db.Column(db.Integer)#int,
    id_area = db.Column(db.Integer)#int,
    salary = db.Column(db.Float)#float,
    haring_date = db.Column(db.DateTime)#date NOT NULL,
    hr_week = db.Column(db.Integer)#int,
    hr_day = db.Column(db.Integer)#int,


    def __init__(self, name, last_names, rfc, curp, birthday, contact_number, address, email, id_position, id_area, salary, haring_date, hr_week, hr_day) -> None:
        self.name = name 
        self.last_names = last_names 
        self.rfc = rfc 
        self.curp = curp 
        self.birthday = birthday 
        self.contact_number = contact_number 
        self.address = address 
        self.email = email 
        self.id_position = id_position 
        self.id_area = id_area 
        self.salary = salary 
        self.haring_date = haring_date 
        self.hr_week = hr_week
        self.hr_day = hr_day

    def __str__(self) -> str:
        return """ 
            Datos del Trabajador:
                id: {}
                Nombre: {}
        """.format(self.id, self.name)


class RegisterWorkerForm(FlaskForm):
    name = StringField('Nombre', validators = [InputRequired(), Length(max=20)] )
    last_names = StringField('Apellido', validators = [InputRequired(), Length(max=40)] )
    rfc = StringField('RFC', validators = [InputRequired(), Length(max=13)] )
    curp = StringField('CURP', validators = [InputRequired(), Length(max=18)] )
    #birthday = DateField('Fecha de nacimiento', validators = [InputRequired()], widget=DatePickerWidget())
    #birthday = DateField('Fecha de nacimiento', validators = [InputRequired()], format='%d/%m/%Y',id='datepick')
    birthday = DateField('Fecha de nacimiento', validators = [InputRequired()], format='%d/%m/%Y')
    contact_number = StringField('Número de teléfono', validators = [InputRequired(), Length(min = 8, max = 10), Regexp(regex = '[0-9]+')])
    address = StringField('Dirección', validators = [InputRequired(), Length(max=64)] )
    email = StringField('Correo electrónico', validators = [InputRequired(), Length(max=32)] )
    id_position = IntegerField('Posición', validators=[Optional()])
    id_area = IntegerField('Área', validators=[Optional()])
    salary = FloatField('Salario', validators=[Optional()])
    haring_date = DateField('Fecha de contratación', validators = [InputRequired()], format='%d/%m/%Y')
    hr_week = IntegerField('Horas a la semana', validators=[ Optional(), NumberRange(min = 0, max = 48) ])
    hr_day = IntegerField('Horas al día', validators=[ Optional(), NumberRange(min = 0, max = 8) ])