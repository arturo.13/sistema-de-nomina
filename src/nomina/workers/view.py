from flask import Blueprint, render_template, flash, redirect, url_for

from nomina import db
from nomina.workers.models.workers import Worker, RegisterWorkerForm

workers_c = Blueprint('workers', __name__)

@workers_c.route('/register/workers', methods=['POST', 'GET'])
def register():
    form = RegisterWorkerForm(meta = {'csrf': False})
    print("Hola")
    if form.validate_on_submit():
        print("valido!!!!")
        worker = Worker(
            email=form.email.data,
            address=form.address.data,
            birthday=form.birthday.data,
            contact_number=form.contact_number.data,
            curp=form.curp.data,
            haring_date=form.haring_date.data,
            hr_day=form.hr_day.data,
            hr_week=form.hr_week.data,
            id_area=form.id_area.data,
            id_position=form.id_position.data,
            last_names=form.last_names.data,
            name=form.name.data,
            rfc=form.rfc.data,
            salary=form.salary.data
        )
        try:
            db.session.add(worker)
            db.session.commit()
            flash('Usuario creado con éxito!')
            return redirect(url_for('home.home_init'))
        except Exception as e:
            print(e)
            flash('Error a la hora de crear al trabajador! Vuelva a intentarlo', 'danger')
            db.session.rollback()
            return redirect(url_for('workers.register'))
    if form.errors:
        flash(form.errors, 'danger')
        print(form.errors)
    return render_template('workers/register.html', form = form)