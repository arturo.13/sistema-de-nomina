from nomina import db

class Area(db.Model):
    """
    ____________________ Data in DB
    id serial NOT NULL,
    name varchar(32) NOT NULL,
    description varchar(256),
    """
    ## .... 
    __tablename__ = 'areas'
    ## ..... variables ...
    #Varibales: db.Column(db.Integer), db.Column(db.Float), db.Column(db.DateTime), db.Column(db.String(8)), db.Column(db.Date), db.Column(db.Boolean)
    id = db.Column(db.Integer, primary_key = True) #serial NOT NULL,
    name = db.Column(db.String(32)) # varchar(32) NOT NULL,
    description = db.Column(db.String(256)) # varchar(256),


    def __init__(self, name, description) -> None:
        self.name = name 
        self.description = description 

    def __str__(self) -> str:
        return """ 
            Datos del Area:
                id:{}
                Nombre: {}
                Descripción: {}
        """.format(self.id, self.name, self.description)