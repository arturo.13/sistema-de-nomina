from nomina import db

class ModelName(db.Model):
    """
    ____________________ Data in DB
    """
    ## .... 
    __tablename__ = ''
    ## ..... variables ...
    #Varibales: db.Column(db.Integer), db.Column(db.Float), db.Column(db.DateTime), db.Column(db.String(8)), db.Column(db.Date), db.Column(db.Boolean)
    id = db.Column(db.Integer, primary_key = True) #serial NOT NULL,


    def __init__(self) -> None:
        pass 

    def __str__(self) -> str:
        return """ 
            Datos del Modelo:
                Dato 1: {}
        """.format(0)