from flask import Blueprint, render_template, flash, redirect, url_for

from nomina import db
from nomina.login.models.users import User, RegsiterForm, LoginForm

login = Blueprint('login', __name__)

@login.route('/register/user', methods=['POST', 'GET'])
def register():
    form = RegsiterForm(meta={'csrf': False})
    if form.validate_on_submit():
        print("Valudo!!")
        user = User(
            name= form.name.data,
            passhash= form.passw.data,
            salt=0,
            id_role=form.id_role.data,
            id_worker= form.id_worker.data
        ) 
        try:
            db.session.add(user)
            db.session.commit()
            flash('Datos Guardados en la BD!!!')
            return redirect( url_for('home.home_init') )
        except Exception as e:
            print(e)
            db.session.rollback()
            flash('Error al guardar en la BD!! Vuelva a intentarlo!', 'danger')
            return redirect( url_for('login.register') )

    if form.errors:
        flash(form.erros, 'danger')
        return redirect( url_for('login.register') )
    return render_template('fauth/register.html', form = form)

@login.route('/login', methods=['POST', 'GET'])
def login_():
    form = LoginForm(meta={'csrf': False})
    if form.validate_on_submit():
        print("Valido!!!")
    if form.errors:
        flash(form.errors, 'danger')
    return render_template('fauth/login.html', form = form)