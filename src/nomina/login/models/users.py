from nomina import db

from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, IntegerField, HiddenField
from wtforms.validators import DataRequired, InputRequired, EqualTo
from werkzeug.security import check_password_hash, generate_password_hash

class User(db.Model):
    """
    ____________________ Data in DB
    id serial NOT NULL,
    name varchar(16) NOT NULL,
    passhash varchar(256) NOT NULL,
    salt varchar(8) NOT NULL,
    id_role int NOT NULL,
    id_worker int NOT NULL,
    """
    ## .... 
    __tablename__ = 'users'
    ## ..... variables ...
    #Varibales: db.Column(db.Integer), db.Column(db.Float), db.Column(db.DateTime), db.Column(db.String(8)), db.Column(db.Date), db.Column(db.Boolean)
    id = db.Column(db.Integer, primary_key = True) #serial NOT NULL,
    name = db.Column(db.String(16)) #varchar(16) NOT NULL,
    passhash = db.Column(db.String(256)) #varchar(256) NOT NULL,
    salt = db.Column(db.String(8)) #varchar(8) NOT NULL,
    id_role = db.Column(db.Integer) #int NOT NULL,
    id_worker = db.Column(db.Integer) #int NOT NULL,


    def __init__(self, name, passhash, salt, id_role, id_worker) -> None:
        self.name = name
        self.passhash = generate_password_hash(passhash)#passhash
        self.salt = salt
        self.id_role = id_role
        self.id_worker = id_worker
    
    @property
    def is_authenticated(self):
        return True 
    
    @property
    def is_active(self):
        return True
    
    @property
    def is_anonymous(self):
        return False 

    def get_id(self):
        return str(self.id)
    
    def check_password(self, password):
        return check_password_hash(self.passhash, password) 

    def __repr__(self) -> str:
        return 'User: %r'%(self.name)

    def __str__(self) -> str:
        return """ 
            Datos del Usuario:
                id: {}
                Nombre: {}
        """.format(self.id, self.name)

class RegsiterForm(FlaskForm):
    """
    ____________________ Data in DB
    id serial NOT NULL,
    name varchar(16) NOT NULL,
    passhash varchar(256) NOT NULL,
    salt varchar(8) NOT NULL,
    id_role int NOT NULL,
    id_worker int NOT NULL,
    """
    name = StringField('NickName', validators=[InputRequired()])
    passw = PasswordField('Contraseña', validators = [InputRequired(), EqualTo('confirm')])
    confirm = PasswordField('Repetir Contraseña')
    id_role = IntegerField('Role', validators = [InputRequired() ])
    id_worker = IntegerField('ID Trabajador', validators = [InputRequired() ])

class LoginForm(FlaskForm):
    name = StringField('NickName', validators=[InputRequired()])
    password = PasswordField('Contraseña', validators=[InputRequired()])
    next = HiddenField('next')