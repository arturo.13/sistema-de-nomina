from flask import *
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)


app.config.from_object('config.DevelopmentConfig')
db = SQLAlchemy(app)

from nomina.home.home_controller import home
from nomina.workers.view import workers_c
from nomina.login.view import login

app.register_blueprint(home)
app.register_blueprint(workers_c)
app.register_blueprint(login)

db.create_all()