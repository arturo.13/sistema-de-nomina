from flask import Blueprint, render_template 

from nomina.areas.models.areas import Area
from nomina.roles.models.roles import Role
from nomina.work_position.models.work_positions import WorkPositions
from nomina.workers.models.workers import Worker
from nomina.login.models.users import User


home = Blueprint('home', __name__)

@home.route('/')
def home_init():
    print("------ Areas:")
    areas = Area.query.all()
    for area in areas:
        print(area)

    print("------ Roles:")
    roles = Role.query.all()
    for role in roles:
        print(role)

    print("------ Posiciones de Trabajo:")
    workPositions = WorkPositions.query.all()
    for wp in workPositions:
        print(wp)

    return render_template('home/home.html', datos = 'DATo!')