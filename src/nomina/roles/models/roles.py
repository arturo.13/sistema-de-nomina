from nomina import db

class Role(db.Model):
    """
    ____________________ Data in DB
    id serial NOT NULL,
    name varchar(32),
    """
    ## .... 
    __tablename__ = 'roles'
    ## ..... variables ...
    #Varibales: db.Column(db.Integer), db.Column(db.Float), db.Column(db.DateTime), db.Column(db.String(8)), db.Column(db.Date), db.Column(db.Boolean)
    id = db.Column(db.Integer, primary_key = True) #serial NOT NULL,
    name = db.Column(db.String(32)) # varchar(32) NOT NULL,


    def __init__(self, name) -> None:
        self.name = name

    def __str__(self) -> str:
        return """ 
            Datos del Role:
                id: {}
                Nombre: {}
        """.format(self.id, self.name)