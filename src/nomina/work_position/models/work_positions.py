from nomina import db

class WorkPositions(db.Model):
    """
    ____________________ Data in DB
    id serial NOT NULL,
    name varchar(32) NOT NULL,
    description varchar(256),
    level_access int NOT NULL, --- 0,1,2 (- ->+)
    """
    ## .... 
    __tablename__ = 'workpositions'
    ## ..... variables ...
    #Varibales: db.Column(db.Integer), db.Column(db.Float), db.Column(db.DateTime), db.Column(db.String(8)), db.Column(db.Date), db.Column(db.Boolean)
    id = db.Column(db.Integer, primary_key = True) #serial NOT NULL,
    name = db.Column(db.String(32)) # varchar(32) NOT NULL,
    description = db.Column(db.String(256)) # varchar(256),
    level_access = db.Column(db.Integer) # int NOT NULL, --- 0,1,2 (- ->+)


    def __init__(self, name, description, level_access) -> None:
        self.name = name 
        self.description = description 
        self.level_access = level_access

    def __str__(self) -> str:
        return """ 
            Datos del Posición de Trabajo:
                id:{}
                Nombre: {}
                Descripción: {}
                Nivel de acceso: {}
        """.format(self.id, self.name, self.description, self.level_access)